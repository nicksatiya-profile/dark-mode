'use strict'

const DARK_THEME = 'dark';
const LIGHT_THEME = 'light';

const toggleSwitch = document.querySelector('input[type="checkbox"]');
const nav = document.getElementById('nav');
const toggleIcon = document.getElementById('toggle-icon');
const image1 = document.getElementById('image1');
const image2 = document.getElementById('image2');
const image3 = document.getElementById('image3');
const textBox = document.getElementById('text-box');
const themeStorageVar = 'theme';


const currentTheme = {
    getTheme: function () {
        return localStorage.getItem(themeStorageVar);
    },
    saveTheme: function (saveTheme) {
        localStorage.setItem(themeStorageVar, saveTheme);
    }
};

const setInitToggleSwitchPos = () => toggleSwitch.checked = (currentTheme.getTheme() === DARK_THEME) ? true : false;

const startUpTheme = () => {

    if (!currentTheme.getTheme()) {
        currentTheme.saveTheme(LIGHT_THEME);
    }

    setInitToggleSwitchPos();
    setTheme(currentTheme.getTheme());
}

const setTheme = (theme) => {

    currentTheme.saveTheme(theme);

    document.documentElement.setAttribute('data-theme', theme);
    image1.src = `img/undraw_proud_coder_${theme}.svg`;
    image2.src = `img/undraw_feeling_proud_${theme}.svg`;
    image3.src = `img/undraw_conceptual_idea_${theme}.svg`;
    toggleIcon.children[0].textContent = `${theme.charAt(0).toUpperCase() + theme.slice(1)} mode`;

    nav.style.backgroundColor = (theme === DARK_THEME) ? 'rgb(0 0 0 / 50%)' : 'rgb(255 255 255 / 50%)';
    textBox.style.backgroundColor = (theme === DARK_THEME) ? 'rgb(255 255 255 / 50%)' : 'rgb(0 0 0 / 50%)';

    (theme === DARK_THEME) ? toggleIcon.children[1].classList.replace('fa-sun', 'fa-moon') :
        toggleIcon.children[1].classList.replace('fa-moon', 'fa-sun');
}

// Do the startup check first
startUpTheme();

toggleSwitch.addEventListener('change', (event) =>
    (event.target.checked === true) ? setTheme(DARK_THEME) : setTheme(LIGHT_THEME));

